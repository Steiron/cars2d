﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUI : Singeltons<PlayerStatsUI>
{
    public Slider ExpSlider;
    public Image ExpFillImage;
    public Text ExpText;

    [Header("Enery UI")] public Slider EnergySlider;
    public Image EneryFillImage;
    public Image EneryBGFillImage;
    public Color FullEneryColor = Color.yellow;
    public Color ZeroEneryColor = Color.white;


    private RectTransform _rectEnergy;

    void Awake()
    {
        _rectEnergy = EnergySlider.GetComponent<RectTransform>();
        _rectEnergy.sizeDelta = new Vector2(0, _rectEnergy.sizeDelta.y);
    }

    public void SetEnergyUI(float energy, float maxEnergy, float maxEnergyLastLevel)
    {
        EnergySlider.value = energy;
        EnergySlider.maxValue = maxEnergy;
        EneryFillImage.color = Color.Lerp(ZeroEneryColor, FullEneryColor, energy / maxEnergy);
        if (_rectEnergy.sizeDelta.x < 1f)
        {
            _rectEnergy.sizeDelta = new Vector2(maxEnergy / maxEnergyLastLevel, _rectEnergy.sizeDelta.y);
        }
    }

    public void SetExp(float mass, float maxMass)
    {
        if (maxMass == 0)
        {
            ExpSlider.gameObject.SetActive(false);
        }
        ExpSlider.value = mass;
        ExpSlider.maxValue = maxMass;

        ExpText.text = string.Format("{0}/{1}",mass,maxMass);
    }
}