﻿using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Score : Singeltons<Score>
{
    public Text ScoreText;
    public Text PlayerList;
    public Text ScoreList;

    void Start()
    {
        InvokeRepeating("UpdateLeaderBoard", 0, 2);
    }

    public void SetScore(float value)
    {
        ScoreText.text = string.Format("Ваши очки: {0}", value);
    }

    private void UpdateLeaderBoard()
    {
        StringBuilder nameBuilder = new StringBuilder();
        StringBuilder scoreBuilder = new StringBuilder();
        int count = 0;
        foreach (TopPlayer player in PlayersManager.Instance.TopPlayers())
        {
            count++;
            nameBuilder.AppendFormat("<color=#{0}>#{1}:\t{2}</color>", player.Color, count, player.playerName);
            nameBuilder.AppendLine();

            scoreBuilder.AppendFormat("<color=#{0}> {1}</color>", player.Color, player.playerScore);
            scoreBuilder.AppendLine();
        }
        PlayerList.text = nameBuilder.ToString();
        ScoreList.text = scoreBuilder.ToString();
    }
}
