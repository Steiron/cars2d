﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Skidmark : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    public float trailTime;

    private float oldRotBack;
    private float oldRotFront;

    public ParticleSystem dust;

    public ParticleSystem[] skids;

    void Awake()
    {
        _rigidbody2D = GetComponentInParent<Rigidbody2D>();
    }

    void Start()
    {
#if server
        if (_rigidbody2D.GetComponent<NetworkIdentity>().isServer)
            Destroy(gameObject);
#endif
    }

    private float time;

    void LateUpdate()
    {
        if (Mathf.Abs(_rigidbody2D.rotation - oldRotBack) > 55)
        {
            oldRotBack = _rigidbody2D.rotation;
            dust.Play();
            skids[0].Play();
            skids[1].Play();
            time = Time.time + 1;
        }
        if (Mathf.Abs(_rigidbody2D.rotation - oldRotFront) > 75)
        {
            oldRotFront = _rigidbody2D.rotation;
            skids[2].Play();
            skids[3].Play();
        }
        if (Time.time > time)
        {
            dust.Stop();
            skids[0].Stop();
            skids[1].Stop();
            skids[2].Stop();
            skids[3].Stop();
        }
    }
}