﻿using UnityEngine;
using UnityEngine.Networking;

public enum CharacterType : byte
{
    None,
    Type1,
    Type2,
    Type3,
    Type4
}

public partial class PlayerStats : NetworkBehaviour
{
    [SerializeField] [SyncVar(hook = "HealthChange")] private float _health;
    [SerializeField] [SyncVar] private float _startHealth;
    [SerializeField] [SyncVar] private float _maxHealth;

    [SerializeField] [SyncVar(hook = "EnergyChange")] private float _energy;
    [SerializeField] [SyncVar] private float _startEnergy;
    [SerializeField] [SyncVar] private float _maxEnergy;

    [SerializeField] [SyncVar(hook = "ChangeMass")] private int _mass;
    [SerializeField] [SyncVar] private float _speed;
    [SerializeField] [SyncVar] private float _boost;
    [SerializeField] [SyncVar] private float _rotationLerp;
    [SerializeField] [SyncVar(hook = "ChangeSpriteClient")] private string _spriteName;

    [SerializeField] [SyncVar(hook = "ChangeNickname")] private string _playerName;

    [SerializeField] private byte _damage;
    [SerializeField] private byte _startDamage;
    [SerializeField] private byte _maxDamage;

    [SerializeField] private float _damageCD;

    [SyncVar] public CharacterType Type = CharacterType.None;

    [SerializeField] [SyncVar] private bool _isBoost;
    public PlayerUI PlayerUi;
    public ParticleSystem Explosion;

    private SpriteRenderer _spriteRenderer;
    [SerializeField] private GameObject _colliders;

    [SyncVar] public bool isActive;
    [SerializeField] private static float _cameraSize;

    [SyncVar] public Colors Colors = Colors.None;

    public Sprite CurrentSprite
    {
        get { return _spriteRenderer.sprite; }
        set { _spriteRenderer.sprite = value; }
    }

    public string SpriteName
    {
        get { return _spriteName; }
#if server
        set
        {
            if(!isServer) return;
            _spriteName = value;
        }
#endif
    }

    #region Health

    public float Health
    {
        get { return _health; }
#if server
        set
        {
            if (!isServer) return;
            _health = value;
            _health = _health > StartHealth ? StartHealth : value;
        }
#endif
    }

    public float StartHealth
    {
        get { return _startHealth; }
#if server
        set
        {
            if (!isServer) return;
            _startHealth = value;
            _startHealth = _startHealth > _maxHealth ? _maxHealth : value;
        }
#endif
    }

    public float MaxHealth
    {
        get { return _maxHealth; }
#if server
        set
        {
            if (!isServer) return;
            _maxHealth = value;
        }
#endif
    }

    #endregion

    #region Energy

    public float Energy
    {
        get { return _energy; }
#if server
        set
        {
            if (!isServer) return;
            _energy = value;
            _energy = _energy > StartEnergy ? StartEnergy : value;
        }
#endif
    }

    public float StartEnergy
    {
        get { return _startEnergy; }
#if server
        set
        {
            if (!isServer) return;
            _startEnergy = value;
            _startEnergy = _startEnergy > MaxEnergy ? MaxEnergy : value;
        }
#endif
    }

    public float MaxEnergy
    {
        get { return _maxEnergy; }
#if server
        set
        {
            if (!isServer) return;
            _maxEnergy = value;
        }
#endif
    }

    #endregion

    #region Speed

    public float Speed
    {
        get { return _speed; }
#if server
        set
        {
            if (!isServer) return;
            _speed = value;
        }
#endif
    }

    public float Boost
    {
        get { return _boost; }
#if server
        set
        {
            if (!isServer) return;
            _boost = value;
        }
#endif
    }

    public bool IsBoost
    {
        get { return _isBoost; }
        set
        {
            if (!isServer) return;
            _isBoost = (Energy > 0) && value;
        }
    }

    #endregion

    #region Damage

    public byte Damage
    {
        get { return _damage; }
#if server
        set
        {
            if (!isServer) return;
            _damage = value;
            _damage = _damage > StartDamage ? StartDamage : value;
        }
#endif
    }

    public byte StartDamage
    {
        get { return _startDamage; }
#if server
        set
        {
            if (!isServer) return;
            _startDamage = value;
            _startDamage = _startDamage > MaxDamage ? MaxDamage : value;
        }
#endif
    }

    public byte MaxDamage
    {
        get { return _maxDamage; }
#if server
        set
        {
            if (!isServer) return;
            _maxDamage = value;
        }
#endif
    }

    public float DamageCD
    {
        get { return _damageCD; }
#if server
        set
        {
            if (!isServer) return;
            _damageCD = value;
        }
#endif
    }

    #endregion

    #region OtherStats

    public int Mass
    {
        get { return _mass; }
#if server
        set
        {
            if (!isServer) return;
            _mass = value;
        }
#endif
    }

    public float RotationLerp
    {
        get { return _rotationLerp; }
#if server
        set
        {
            if (!isServer) return;
            _rotationLerp = value;
        }
#endif
    }

    public string PlayerName
    {
        get { return _playerName; }
#if server
        set
        {
            if (!isServer) return;
            _playerName = value;
        }
#endif
    }

    public static float CameraSize
    {
        get { return _cameraSize; }
        private set
        {
            _cameraSize = value;
            _cameraSize = Mathf.Clamp(_cameraSize, 4, 10);
        }
    }

    #endregion

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void ChangeSpriteClient(string newSprite)
    {
        CurrentSprite = Resources.Load<Sprite>(string.Format("Sprites/Car/{0}/{1}", Type, newSprite));
        _spriteName = newSprite;
        SpriteName = newSprite;

#if client
        if (isLocalPlayer)
            SetClientUI();
#endif
    }

    private void HealthChange(float newHealth)
    {
        _health = newHealth;
        PlayerUi.SetHealthUI();
    }

    private void EnergyChange(float newEnergy)
    {
        _energy = newEnergy;
    }

    private void ChangeNickname(string newName)
    {
        _playerName = newName;
        PlayerUi.SetName();
        /*  if (isLocalPlayer)
              PlayerUi.SetName();*/
    }

    private void ChangeMass(int newMass)
    {
        _mass = newMass;
#if client
        if (isLocalPlayer)
            SetClientUI();
#endif
    }

    [Command]
    public void Cmd_PlayerActive()
    {
        isActive = true;
    }

    [Command]
    public void Cmd_SetCameraSize(float value)
    {
        CameraSize = value;
    }

    [ClientRpc]
    public void Rpc_Explosion()
    {
        Explosion.Play();
    }

    [ClientRpc]
    public void Rpc_ActivePlayer(bool active)
    {
        _spriteRenderer.enabled = active;
        _colliders.SetActive(active);

        PlayerUi.PlayerNameText.gameObject.SetActive(active);
        PlayerUi.HealthSlider.gameObject.SetActive(active);
    }
}