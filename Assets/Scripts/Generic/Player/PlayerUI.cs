﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] public PlayerStats _playerStats;

    [SerializeField] private GameObject _HealthCanvas;
    [SerializeField] private GameObject _PlayerNameCanvas;

    [Header("Health UI")] public Slider HealthSlider;
    public Image HealthFillImage;
    public Image HealthBGFillImage;
    public Color FullHealthColor = Color.green;
    public Color ZeroHealthColor = Color.red;


    [Header("PlayerName UI")] public Text PlayerNameText;

    public float MaxSize = 2f;
    private RectTransform _rectHealth;

    void Awake()
    {
        //_playerStats = GetComponent<PlayerStats>();
        _rectHealth = HealthSlider.GetComponent<RectTransform>();
        _rectHealth.sizeDelta = new Vector2(0, _rectHealth.sizeDelta.y);
    }

#if server
   public void DestoyComp()
    {
        if (!_playerStats.GetComponent<NetworkIdentity>().isServer || _HealthCanvas == null || _PlayerNameCanvas == null)
            return;
        Destroy(_HealthCanvas);
        Destroy(_PlayerNameCanvas);
    }
#endif
    

    public void SetHealthUI()
    {
        if(HealthSlider == null) return;
        HealthSlider.value = _playerStats.Health;
        HealthSlider.maxValue = _playerStats.StartHealth;
        Debug.LogError(string.Format("{0}/{1}", HealthSlider.value, HealthSlider.maxValue));
        HealthFillImage.color = Color.Lerp(ZeroHealthColor, FullHealthColor, _playerStats.Health/_playerStats.StartHealth);
        if (_rectHealth.sizeDelta.x < MaxSize)
        {
            _rectHealth.sizeDelta = new Vector2(_playerStats.StartHealth/_playerStats.MaxHealth, _rectHealth.sizeDelta.y);
        }
    }

    public void SetName()
    {
        if (PlayerNameText == null) return;
        PlayerNameText.text = _playerStats.PlayerName;

        Debug.LogError(PlayerNameText.text);
    }
}