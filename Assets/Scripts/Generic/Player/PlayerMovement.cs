﻿using UnityEngine;
using UnityEngine.Networking;

//Base Generic PlayerMovement class

public partial class PlayerMovement : NetworkBehaviour
{
    public GameObject playerCamera;
    public GameObject MainPlayerUI;
    public GameObject TriggerColl;
    public GameObject Background;

    [SyncVar] private Quaternion syncPlayerRotation;

    private Rigidbody2D _rigidbody2D;

    public PlayerStats PlayerStats;


    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public virtual void FixedUpdate()
    {
        if (!PlayerStats.isActive) return;
        _rigidbody2D.isKinematic = PlayerStats.Speed <= 0;
#if server
        if (isServer)
        {
            TransformServer();
        }
#endif

#if client

        if (isLocalPlayer)
        {
            TransmitRotation();
            CmdBoost(Input.GetMouseButton(0));

            ChangeCamera();
        }

#endif
    }


    [Command]
    void CmdBoost(bool value)
    {
        PlayerStats.IsBoost = value;
    }

    [Command]
    void CmdServerUpdateRotation(Quaternion rotation)
    {
        syncPlayerRotation = rotation;
    }
    
}