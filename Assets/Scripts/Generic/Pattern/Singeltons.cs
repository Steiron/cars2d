﻿using UnityEngine;

public class Singeltons<T> : MonoBehaviour where T : MonoBehaviour, new()
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            Instantiate();
            return _instance;
        }
    }

    public static void Instantiate()
    {
        if (_instance != null) return;
        _instance = (T)FindObjectOfType(typeof(T)) ?? new T();
    }
}