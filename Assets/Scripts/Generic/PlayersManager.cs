﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;


public enum Colors
{
    None,
    _ff0000, // Red
    _008000, //Green
    _0000ff, // Blue
    _ffff00, // Yellow
    _000000, // Black
    _ffffff, // White
    _800080, // Purple
    _00ffff, // Aqua
    _66ff00, // Bright green
    _fdee00, // Aureolin
    _00ff00, // Electric green
    _ff004f, // Folly
}

[System.Serializable]
public struct PlayerInfo
{
    public NetworkInstanceId Id;
    public string Name;
    public Colors Color;
    public int Mass;
    
#if server
    public PlayerInfo(NetworkInstanceId id, string name, Colors color, int mass)
    {
        Id = id;
        Name = name;
        Color = color;
        Mass = mass;
    }
#endif
}

[System.Serializable]
public struct TopPlayer
{
    private string _playerName;

    public string playerName
    {
        get { return _playerName; }
        set
        {
            if (value.Length > 26)
                value = value.Substring(0, 26);
            _playerName = value;
        }
    }

    public int playerScore;
    public string Color;
}

public class PlayersManager : NetworkBehaviour
{
    public static PlayersManager Instance;

    public class PlayerInfoList : SyncListStruct<PlayerInfo>
    {
    }

    [SerializeField] public PlayerInfoList _playerInfoList = new PlayerInfoList();

#if server
    void Awake()
    {
        Instance = this;

        InvokeRepeating("UpdateListInfo", 0, 4);
    }

    private List<PlayerInfo> TempList = new List<PlayerInfo>();

    private void UpdateListInfo()
    {
        if (!isServer) return;

        TempList = _playerInfoList.ToList();
        foreach (PlayerInfo playerInfo in TempList)
        {
            GameObject playerObj = NetworkServer.FindLocalObject(playerInfo.Id);
            if (playerObj == null)
            {
                if (_playerInfoList.Contains(playerInfo)) _playerInfoList.Remove(playerInfo);
                continue;
            }
            var stats = playerObj.GetComponent<PlayerStats>();
            if (stats == null) continue;

            int index = _playerInfoList.IndexOf(playerInfo);
            _playerInfoList[index] = new PlayerInfo(playerInfo.Id, stats.PlayerName, playerInfo.Color, stats.Mass);
        }

        TempList.Clear();
    }

    public void AddPlayer(GameObject player, int id, string name, Colors colors)
    {
        if (player == null)
        {
            Debug.LogError("Player Null");
            return;
        }

        NetworkIdentity uv = player.GetComponent<NetworkIdentity>();
        PlayerStats stats = player.GetComponent<PlayerStats>();
        stats.Colors = colors;
        stats.PlayerName = name;

        _playerInfoList.Add(new PlayerInfo(uv.netId, stats.PlayerName, stats.Colors, stats.Mass));
    }

    public void RemovePlayer(GameObject player)
    {
        if (player == null)
        {
            Debug.LogError("Player Null");
            return;
        }

        var uv = player.GetComponent<NetworkIdentity>();
        PlayerInfo playerInfo = new PlayerInfo();
        foreach (PlayerInfo info in _playerInfoList)
        {
            if (info.Id != uv.netId) continue;
            playerInfo = info;
        }
        _playerInfoList.Remove(playerInfo);
    }

#endif

    public IEnumerable TopPlayers()
    {
        return _playerInfoList.Select(p => new TopPlayer {playerName = p.Name, playerScore = p.Mass, Color = p.Color.ToString().Replace("_", "")})
            .Where(m => m.playerScore > 0)
            .OrderByDescending(o => o.playerScore)
            .Take(10);
    }
}