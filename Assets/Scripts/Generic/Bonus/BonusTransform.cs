﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BonusTransform : NetworkBehaviour
{
    private Transform _myTransform;
    [SerializeField] [SyncVar(hook = "ChangeColor")] private Color _color;
    [SerializeField] [SyncVar(hook = "ChangeSprite")] private string _spiteName;
    [SerializeField] [SyncVar(hook = "ChangeBonusName")] private string _bonusName;
    private SpriteRenderer _spriteRenderer;

    public Color MyColor
    {
        get { return _spriteRenderer.color; }
        set
        {
            if (_spriteRenderer != null) _spriteRenderer.color = value;
        }
    }

    public Color BonusColor
    {
        get { return _color; }
        set
        {
            if (!isServer) return;
            _color = value;
        }
    }

    public Sprite CurrentSprite
    {
        get { return _spriteRenderer.sprite; }
        set { _spriteRenderer.sprite = value; }
    }

    public string CurrentSpriteName
    {
        get { return _spiteName; }
        set
        {
            if (!isServer) return;
            _spiteName = value;
        }
    }

    public string BonusName
    {
        get { return _bonusName; }
        set
        {
            if (!isServer) return;
            _bonusName = value;
        }
    }

    void Start()
    {
        _myTransform = transform;
        _spriteRenderer = GetComponent<SpriteRenderer>();
#if server
        BonusColor = MyColor;
        CurrentSpriteName = CurrentSprite.name;
        BonusName = _myTransform.name;
#endif
#if client
        MyColor = BonusColor;
        LoadSpriteByName();
        _myTransform.name = BonusName;
#endif
    }

    public void ChangePosition(Vector2 position)
    {
        if (!isServer) return;

        BonusColor = MyColor;
        CurrentSpriteName = CurrentSprite.name;

        Rpc_Update(position);
        Rpc_UpdateColor();
        Rpc_UpdateSprite();
    }

    void ChangeColor(Color newColor)
    {
        _color = newColor;
        MyColor = newColor;
    }

    void ChangeSprite(string newSpriteName)
    {
        _spiteName = newSpriteName;
        LoadSpriteByName();
    }

    void ChangeBonusName(string newName)
    {
        _bonusName = newName;
        _myTransform.name = newName;
    }

    private void LoadSpriteByName()
    {
        CurrentSprite = Resources.Load<Sprite>("Sprites/Bonus/" + _spiteName);
    }

    #region RPC

    [ClientRpc(channel = 1)]
    void Rpc_Update(Vector2 position)
    {
        if (!isClient) return;
        if (_myTransform == null) return;
        _myTransform.position = position;
        _myTransform.name = BonusName;
        
        if(isServer) return;
       // MiniMap.Instance.UpdateBonusOnMiniMap(BonusName);
    }

    [ClientRpc(channel = 1)]
    void Rpc_UpdateColor()
    {
        if (!isClient) return;
        MyColor = BonusColor;
    }

    [ClientRpc(channel = 1)]
    void Rpc_UpdateSprite()
    {
        if (!isClient) return;
        LoadSpriteByName();
    }

    #endregion
}