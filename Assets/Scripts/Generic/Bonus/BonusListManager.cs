﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Networking;

public struct BonusList
{
    public NetworkInstanceId Id;
    public Vector2 Position;
    public string SpriteName;

    public BonusList(GameObject gameObject)
    {
        Id = gameObject.GetComponent<NetworkIdentity>().netId;
        Position = gameObject.transform.localPosition;
        SpriteName = gameObject.GetComponent<SpriteRenderer>().sprite.name;
    }
}

public class BonusListManager : NetworkBehaviour {

    public class BonusSyncList : SyncListStruct<BonusList> { }
    public BonusSyncList _bonusSync = new BonusSyncList();

    public static BonusListManager Instance;

    void Awake()
    {
        Instance = this;
    }

}
