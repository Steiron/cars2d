﻿#if server
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

public class AITransform : NetworkBehaviour
{
    private PlayerStats _playerStats;
    public GameObject target;

    private Rigidbody2D _rigidbody2D;
    private Transform myTransform;
    private BonusManager _bonusManager;
    private PlayersManager _playersManager;

    void Awake()
    {
        myTransform = transform;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _playerStats = GetComponent<PlayerStats>();
        _bonusManager = BonusManager.Instance;
        _playersManager = PlayersManager.Instance;
        InvokeRepeating("SetTarget", 0, 5);
    }

    private Vector2 dir;

    void Start()
    {
        SetTarget();
    }

    void FixedUpdate()
    {
        _rigidbody2D.isKinematic = _playerStats.Speed < 0;
        if (isServer)
        {
            TransformAI();
        }
    }

    Vector2 Move()
    {
        dir = new Vector2(myTransform.right.x, myTransform.right.y);
        if (_playerStats.IsBoost)
            return dir*_playerStats.Speed*_playerStats.Boost*Time.fixedDeltaTime;
        return dir*_playerStats.Speed*Time.fixedDeltaTime;
    }

    private Vector2 targetVector;

    void TransformAI()
    {
        targetVector = target == null
            ? Vector2.one
            : (Vector2) target.transform.position - _rigidbody2D.position;


        float angle = Mathf.Atan2(targetVector.y, targetVector.x)*Mathf.Rad2Deg;
        var rot = Quaternion.AngleAxis(angle, Vector3.forward);

        myTransform.rotation = Quaternion.Lerp(myTransform.rotation, rot, _playerStats.RotationLerp*Time.fixedDeltaTime);
        _rigidbody2D.MoveRotation(myTransform.rotation.eulerAngles.z);

        _rigidbody2D.AddForce(Move(), ForceMode2D.Impulse);

        _playerStats.IsBoost = _playerStats.Energy > 0 && isBoost;
    }

    public bool CheckHealth()
    {
        if (target == null) return true;
        if (_playerStats.Health < _playerStats.StartHealth/1.5f) return false;
        return !target.CompareTag("Player");
    }

    private bool isBoost;
    private void SetTarget()
    {
        GameObject player = _playersManager._playerInfoList.Select(info => NetworkServer.FindLocalObject(info.Id)).TakeWhile(tmpPlayer => tmpPlayer != null).FirstOrDefault(tmpPlayer => Vector2.Distance(myTransform.position, tmpPlayer.transform.position) < 35 && tmpPlayer != gameObject);

        if (target != null && target == player)
        {
            target = null;
            player = null;
        }

        target = player != null || !CheckHealth()
            ? player
            : _bonusManager.BonusList.FirstOrDefault(
                  d => Vector2.Distance(myTransform.position, d.transform.position) < 4f)
              ??
              _bonusManager.BonusList.FirstOrDefault(
                  o => Vector2.Distance(myTransform.position, o.transform.position) < 10f);
        
        if (target == player)
            isBoost = true;
        else if (_playerStats.Energy > _playerStats.StartEnergy/2)
            isBoost = true;
        else
            isBoost = false;
    }
}
#endif