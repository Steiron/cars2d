﻿#if server
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Linq;

// Attach this to objects that need their visibility updated as the player moves around
[RequireComponent(typeof(NetworkIdentity))]
public class NetworkVisbility : NetworkBehaviour
{
    public List<NetworkConnection> playersObserving = new List<NetworkConnection>();
    public NetworkIdentity networkIdentity;

    void Awake()
    {
        networkIdentity = GetComponent<NetworkIdentity>();
    }

    public override bool OnRebuildObservers(HashSet<NetworkConnection> observers, bool initial)
    {
        foreach (NetworkConnection net in playersObserving)
        {
            observers.Add(net);
        }

        return true;
    }


    public override bool OnCheckObserver(NetworkConnection newObserver)
    {
        return false;
    }
}
#endif