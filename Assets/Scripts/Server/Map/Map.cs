﻿#if server
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class Map : NetworkBehaviour
{
    [SerializeField] private SpriteRenderer mapBG;
    public Vector3 mapSize;
    public int BotCount = 50;
    public static Map Instance;

    void Awake()
    {
        Instance = this;
        mapBG = GetComponentInChildren<SpriteRenderer>();
        mapSize = mapBG.bounds.size;
    }

    public override void OnStartServer()
    {
        StartCoroutine(SpawnBot());
    }


    IEnumerator SpawnBot()
    {
        for (int i = 0; i < BotCount; i++)
        {

            if (!NetworkServer.active) yield break;

            GameObject bot =Instantiate(NetworkManager.singleton.spawnPrefabs.FirstOrDefault(n => n.name.Equals("Bot")),
                    RandomMapSpawnPosition, Quaternion.identity) as GameObject;

            if (bot == null) continue;
            PlayerStats stats = bot.GetComponent<PlayerStats>();
            stats.Type = (CharacterType) Random.Range(1, 3);
           // stats._playerName = "Mr.Bot #" + i;
            //stats.Colors = (Colors) Random.Range(0, 5);
            bot.AddComponent<AITransform>();


            NetworkServer.Spawn(bot);
            PlayersManager.Instance.AddPlayer(bot, i, "Mr.Bot #" + i, (Colors) Random.Range(1, 12));
            yield return new WaitForSeconds(0.25f);
        }
    }

    public Vector3 RandomMapSpawnPosition
    {
        get
        {
            float randomX = Random.Range(-mapSize.x, mapSize.x);
            float randomY = Random.Range(-mapSize.y, mapSize.y);
            return new Vector3(randomX/2.2f, randomY/2.2f, 0);
        }
    }
}
#endif