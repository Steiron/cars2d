﻿#if server
using UnityEngine;
using UnityEngine.Networking;

public class BaseBonusStats : NetworkBehaviour
{
    public float AddHealth { get; private set; }
    public float AddEnergy { get; private set; }
    public int AddScore { get; private set; }
    
    public void SetStats(BonusStats bonusStats)
    {
        AddHealth = Random.Range(bonusStats.AddHealth.x, bonusStats.AddHealth.y);
        AddEnergy = Random.Range(bonusStats.AddEnergy.x, bonusStats.AddEnergy.y);
        AddScore = (int) Random.Range(bonusStats.AddScore.x, bonusStats.AddScore.y);
    }
}
#endif