﻿#if server
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum BonusRarity
{
    Common = 0,
    Uncommon = 35,
    Rare = 75,
    UltraRare = 100
}

public enum BonusType
{
    Heal,
    Energy,
    Score
}

[System.Serializable]
public class BonusStats
{
    public BonusRarity BonusRarity;
    public Vector2 AddHealth;
    public Vector2 AddEnergy;
    public Vector2 AddScore;
    public Color Color;
}

[System.Serializable]
public class Bonus : ScriptableObject
{
    public BonusType BonusType;
    public List<Sprite> Sprites;
    public List<BonusStats> BonusStatses;

    public BonusStats GetBonusStats(int dropWeight)
    {
        return BonusStatses.Last(stats => stats.BonusRarity <= (BonusRarity) dropWeight);
    }

    public string GetName
    {
        get { return BonusType.ToString(); }
    }

    public Sprite GetSprite
    {
        get
        {
            int spriteIndex = Random.Range(0, Sprites.Count);
            return Sprites[spriteIndex];
        }
    }
}
#endif