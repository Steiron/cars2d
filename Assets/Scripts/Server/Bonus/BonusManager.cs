﻿#if server
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class BonusManager : NetworkBehaviour
{
    public static BonusManager Instance;
    public GameObject BonusListGameObject;

    public int maxBonusOnMap = 750;
    public List<GameObject> BonusList = new List<GameObject>();

    [SerializeField] private GameObject parentBonusObject;
    public GameObject BonusGameObjects;

    private int bonusCount;
    private BonusListManager _bonusListManager;

    GameObject bonusList;

    void Awake()
    {
        Instance = this;
        bonusCount = BonusRarityManager.GetListCount;

        bonusList = Instantiate(BonusListGameObject);
        _bonusListManager = BonusListManager.Instance;
    }

    public override void OnStartServer()
    {
        SpawnFirstBonus();
    }

    private void SpawnFirstBonus()
    {
        for (int i = 0; i < maxBonusOnMap; i++)
        {
            GameObject bonus = SpawnBonus(Map.Instance.RandomMapSpawnPosition);
            NetworkServer.Spawn(bonus);

            var id = bonus.GetComponent<NetworkIdentity>().netId;

            bonus.name += string.Format(" - #{0}",id);
        }
        NetworkServer.Spawn(bonusList);
    }

    private GameObject SpawnBonus(Vector2 position, GameObject oldBonus = null, int changing = 0)
    {
        int index = Random.Range(0, bonusCount);
        Bonus bonus = BonusRarityManager.GetBonus(index);
        int rarity = Random.Range(0, 101) + changing;
        BonusStats itemStats = bonus.GetBonusStats(rarity);
        GameObject item = oldBonus ?? Instantiate(BonusGameObjects);

        item.transform.position = position;
        item.name = string.Format("{0} - {1}", bonus.GetName, itemStats.BonusRarity);
        SpriteRenderer itemRenderer = item.GetComponent<SpriteRenderer>();
        itemRenderer.sprite = bonus.GetSprite;
        itemRenderer.color = itemStats.Color;

        BaseBonusStats baseBonus = item.GetComponent<BaseBonusStats>() ?? item.AddComponent<BaseBonusStats>();
        baseBonus.SetStats(itemStats);
        item.transform.SetParent(parentBonusObject.transform);

        if (!BonusList.Contains(item)) BonusList.Add(item);

        if (_bonusListManager._bonusSync.Count == 0 ||
            !_bonusListManager._bonusSync.Select(list => NetworkServer.FindLocalObject(list.Id)).Contains(item) &&
            _bonusListManager._bonusSync.Count < 1900)
            _bonusListManager._bonusSync.Add(new BonusList(item));

        return item;
    }


    private Vector2 RandomPointoPlayer(Vector2 position)
    {
        float randomValX = Random.Range(-4f, 4f);
        float randomValY = Random.Range(-4f, 4f);
        return new Vector2(position.x + randomValX, position.y + randomValY);
    }

    public void RespawnBonus(GameObject bonus)
    {
        SpawnBonus(Map.Instance.RandomMapSpawnPosition/1.25f, bonus);
        bonus.GetComponent<BonusTransform>().ChangePosition(bonus.transform.position);
    }

    public void RespawnBonusAfterPlayer(Vector2 position, int mass)
    {
        int change = Mathf.Clamp(mass/10, 1, Random.Range(30, 75));
        int count = Mathf.Clamp(mass/25, 1, Random.Range(10, 20));

        for (int i = 0; i < count; i++)
        {
            GameObject bonus = SpawnBonus(RandomPointoPlayer(position), null, change);
            bonus.tag = "PlayerBonus";
            NetworkServer.Spawn(bonus);
        }
    }

    public void RemoveBonusInList(GameObject bonus)
    {
        if (BonusList.Contains(bonus)) BonusList.Remove(bonus);

        if (!_bonusListManager._bonusSync.Select(list => NetworkServer.FindLocalObject(list.Id)).Contains(bonus))
            _bonusListManager._bonusSync.Add(new BonusList(bonus));
    }
}

#endif