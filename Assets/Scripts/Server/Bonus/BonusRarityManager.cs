﻿#if server
using UnityEngine;
using System.Collections.Generic;

public class BonusRarityManager
{
    private static List<Bonus> bonusList = new List<Bonus>();

    public static int GetListCount
    {
        get
        {
            Validate();
            return bonusList.Count;
        }
    }

    private static bool _isLoaded = false;

    private static void Validate()
    {
        if (bonusList == null) bonusList = new List<Bonus>();
        if (!_isLoaded) Loaded();
    }

    public static void Loaded()
    {
        if (_isLoaded) return;
        _isLoaded = true;
        LoadForce();
    }

    public static void LoadForce()
    {
        Validate();
        Bonus[] resources = Resources.LoadAll<Bonus>(@"BonusDB");
        foreach (Bonus bonus in resources)
        {
            if (!bonusList.Contains(bonus))
                bonusList.Add(bonus);
        }
    }

    public static Bonus GetBonus(int index)
    {
        Validate();
        return Object.Instantiate(bonusList[index]);
    }
}
#endif