﻿#if server
using UnityEngine;
using UnityEngine.Networking;

//Server Player Movement

public partial class PlayerMovement
{
    private float RotationLerp;

    
    Vector2 Move()
    {
        Vector2 dir = new Vector2(transform.right.x, transform.right.y);
        if (PlayerStats.IsBoost)
            return dir*PlayerStats.Speed*PlayerStats.Boost*Time.fixedDeltaTime;
        return dir*PlayerStats.Speed*Time.fixedDeltaTime;
    }

    void TransformServer()
    {
        if (!isServer) return;


        RotationLerp = PlayerStats.RotationLerp;
        if (PlayerStats.IsBoost)
            RotationLerp /= 1.25f;

        transform.rotation = Quaternion.Lerp(transform.rotation, syncPlayerRotation, RotationLerp*Time.fixedDeltaTime);
        _rigidbody2D.MoveRotation(transform.rotation.eulerAngles.z);
        _rigidbody2D.AddForce(Move(), ForceMode2D.Impulse);
    }
}
#endif