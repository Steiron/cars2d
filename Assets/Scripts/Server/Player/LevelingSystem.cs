﻿#if server
using System.Collections.Generic;
using UnityEngine;

public class LevelingSystem
{
    private static List<PlayerClass> _playerClasses;

    private static bool _isLoaded = false;

    private static void Validate()
    {
        if (_playerClasses == null) _playerClasses = new List<PlayerClass>();
        if (!_isLoaded) Loaded();
    }

    public static void Loaded()
    {
        if (_isLoaded) return;
        _isLoaded = true;
        LoadForce();
    }

    public static void LoadForce()
    {
        Validate();
        PlayerClass[] resouces = Resources.LoadAll<PlayerClass>(@"PlayerClasses");
        foreach (PlayerClass playerClass in resouces)
        {
            if (!_playerClasses.Contains(playerClass))
                _playerClasses.Add(playerClass);
        }
    }


    public static PlayerClass GetPlayerClassByType(CharacterType type)
    {
        Validate();
        foreach (PlayerClass playerClass in _playerClasses)
        {
            if(playerClass.CharacterType == type)
                return Object.Instantiate(playerClass);
        }
        return null;
    }
}
#endif