﻿#if server
using UnityEngine;
using UnityEngine.Networking;


public partial class PlayerStats
{
    private PlayerClass _playerClass;

    public override void OnStartServer()
    {
        gameObject.AddComponent<NetworkVisbility>();
        SetParametrs();

        PlayerUi.DestoyComp();
        Destroy(PlayerUi);
        Destroy(Explosion);
    }

    void Update()
    {
        if (!isServer) return;
        EnergyControll();
    }

    public void SizeUp(float health = 0f, float energy = 0f)
    {
        if (!isServer) return;


        float val = Mass/5000f;
        if (val > 0.5f)
        {
            StartHealth = MaxHealth*val;
            StartEnergy = MaxEnergy*val;
            StartDamage = (byte) (MaxDamage*val);
        }
        else
        {
            StartHealth = _playerClass.StartHealth + MaxHealth*val;
            StartEnergy = _playerClass.StartEnergy + MaxEnergy*val;
            StartDamage = (byte) (_playerClass.StartDamage + MaxDamage*val);
        }
        Health += health;
        Energy += energy;
        Damage = StartDamage;

        /*if (Mass >= MassNextLevel && _playerClass.CurrentLevel < _playerClass.LevelStatses.Length - 1)
        {
            _currentLevel = ++_playerClass.CurrentLevel;
            SetParametrs();
        }*/
    }

    private float time;

    private void EnergyControll()
    {
        if (!isServer) return;

        if (IsBoost)
        {
            Energy -= Time.deltaTime*25f;
            time = Time.time + 3;
        }
        else if (Time.time > time)
            Energy += Time.deltaTime*15f;
    }

    private void SetParametrs()
    {
        if (_playerClass == null)
            _playerClass = LevelingSystem.GetPlayerClassByType(Type);


        MaxHealth = _playerClass.MaxHealth;
        StartHealth = _playerClass.StartHealth;

        MaxEnergy = _playerClass.MaxEnergy;
        StartEnergy = _playerClass.StartEnergy;

        Speed = _playerClass.Speed;
        Boost = _playerClass.Boost;

        MaxDamage = _playerClass.MaxDamage;
        StartDamage = _playerClass.StartDamage;
        DamageCD = _playerClass.DamageCD;

        RotationLerp = _playerClass.RotationLerp;

        _spriteName = _playerClass.SpriteName;

        Health = StartHealth;
        Energy = StartEnergy;
        Damage = StartDamage;
    }


    [Server]
    private void Respawn()
    {
        transform.position = Map.Instance.RandomMapSpawnPosition;
        Type = (CharacterType) Random.Range(1, 3);

        SetParametrs();
        SizeUp();
        Rpc_ActivePlayer(true);
    }

    [Server]
    public void TakeDamage(byte amount)
    {
        if (!isServer) return;

        Health -= amount;

        if (Health <= 0)
            DestoryPlayer();
    }

    [Server]
    void DestoryPlayer()
    {
        if (!isServer) return;
        Speed = 0;
        Mass = 0;
        Rpc_ActivePlayer(false);
        Rpc_Explosion();

        BonusManager.Instance.RespawnBonusAfterPlayer(transform.position, Mass);
        Invoke("Respawn", 5);
    }
}
#endif