﻿#if server
using UnityEngine;

[System.Serializable]
public struct PlayerStates
{
    public float Speed;
    public float Boost;
    public float StartHealth;
    public float MaxHealth;
    public float StartEnergy;
    public float MaxEnergy;
    public float RotationLerp;
    public float DamageCD;
    public byte StartDamage;
    public byte MaxDamage;
    public Sprite Sprite;
}

[System.Serializable]
public class PlayerClass : ScriptableObject
{
    [SerializeField] private CharacterType _type;
    [SerializeField] private PlayerStates _playerStates;

    public CharacterType CharacterType
    {
        get { return _type; }
        set { _type = value; }
    }

    public float Speed
    {
        get { return _playerStates.Speed; }
    }

    public float RotationLerp
    {
        get { return _playerStates.RotationLerp; }
    }

    public string SpriteName
    {
        get { return _playerStates.Sprite.name; }
    }

    public float StartHealth
    {
        get { return _playerStates.StartHealth; }
    }

    public float MaxHealth
    {
        get { return _playerStates.MaxHealth; }
    }

    public float StartEnergy
    {
        get { return _playerStates.StartEnergy; }
    }

    public float MaxEnergy
    {
        get { return _playerStates.MaxEnergy; }
    }

    public float Boost
    {
        get { return _playerStates.Boost; }
    }

    public byte StartDamage
    {
        get { return _playerStates.StartDamage; }
    }

    public byte MaxDamage
    {
        get { return _playerStates.MaxDamage; }
    }

    public float DamageCD
    {
        get { return _playerStates.DamageCD; }
    }
}

#endif