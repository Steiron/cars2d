﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerAttack : MonoBehaviour
{
#if server
    private NetworkIdentity networkIdentity;
    private PlayerStats _playerStats;
    private Rigidbody2D _rigidbody2D;
    private AITransform _ai;
    private BonusManager _bonusManager;

    void Awake()
    {
        networkIdentity = GetComponentInParent<NetworkIdentity>();
        _playerStats = GetComponentInParent<PlayerStats>();
        _rigidbody2D = GetComponentInParent<Rigidbody2D>();
        _bonusManager = BonusManager.Instance;
    }
    
    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (!networkIdentity.isServer) return;
        if (collider2D.CompareTag("Bonus"))
        {
            
            var bonusStats = collider2D.GetComponent<BaseBonusStats>();
            HealOrAddMass(bonusStats.AddHealth, bonusStats.AddEnergy, bonusStats.AddScore);

            _bonusManager.RemoveBonusInList(collider2D.gameObject);
            _bonusManager.RespawnBonus(collider2D.gameObject);
        }

        if (collider2D.CompareTag("PlayerBonus"))
        {
            _bonusManager.RemoveBonusInList(collider2D.gameObject);

            var bonusStats = collider2D.GetComponent<BaseBonusStats>();
            HealOrAddMass(bonusStats.AddHealth, bonusStats.AddEnergy, bonusStats.AddScore);
            NetworkServer.Destroy(collider2D.gameObject);
        }
    }

    private float lastAttack = 0;

    void OnCollisionEnter2D(Collision2D collision2D)
    {
        if (!networkIdentity.isServer) return;


        if (collision2D.collider.CompareTag("Player"))
        {
            var stats = collision2D.gameObject.GetComponentInParent<PlayerStats>();
            if (stats == null) return;

            Vector2 diff = (stats.transform.localPosition - _playerStats.transform.localPosition).normalized;
            collision2D.rigidbody.AddForce(diff*_rigidbody2D.velocity.magnitude*0.5f, ForceMode2D.Impulse);


            if (collision2D.collider.name.Equals("Head")) return;
            if (_rigidbody2D.velocity.magnitude > _playerStats.Speed/1.45f)
            {
                stats.TakeDamage(_playerStats.Damage);
                _playerStats.TakeDamage((byte) (_playerStats.Health*0.1f));
            }
            else
            {
                if (Time.time < lastAttack) return;

                byte b = (byte) Mathf.Abs(_rigidbody2D.velocity.magnitude);
                if (b == 0) return;

                lastAttack = Time.time + _playerStats.DamageCD;

                stats.TakeDamage((byte) ((b*0.25)/_playerStats.Speed*(_playerStats.Damage)));
                _playerStats.TakeDamage((byte) (_playerStats.Health*0.005f*b));
            }

           /* if (_ai != null)
                _ai.CheckHealth();*/
        }
    }

    void HealOrAddMass(float health, float energy, int mass)
    {
        if (!networkIdentity.isServer) return;

        _playerStats.Mass += mass;
        _playerStats.SizeUp(health, energy);
    }

#endif
}