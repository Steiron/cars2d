﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(CircleCollider2D))]
public class Proximity : NetworkBehaviour
{
    private CircleCollider2D col;
#if server
    public List<NetworkVisbility> changedObjects = new List<NetworkVisbility>();
#endif

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        col.radius = 25;
        col.isTrigger = true;
        // Invoke("ChangeCollSize",0.15f);
#if server
        InvokeRepeating("RebuildChangedObjects", 0, 1f);
#endif
    }

#if server
    void RebuildChangedObjects()
    {
        if (!NetworkServer.active)
            return;

        foreach (NetworkVisbility net in changedObjects)
        {
            if (net == null) continue;
            net.networkIdentity.RebuildObservers(false);
        }
        changedObjects.Clear();
    }

#endif

    void OnTriggerEnter2D(Collider2D collider2D)
    {
#if server

        NetworkVisbility net = GetNetworkVisibility(collider2D);
        if (net != null && connectionToClient != null)
        {
            net.playersObserving.Add(connectionToClient);
            changedObjects.Add(net);
        }

#endif
#if client
        if (!isLocalPlayer) return;

        var playerStats = collider2D.GetComponent<PlayerStats>();
        if (playerStats == null) return;
        playerStats.SetAnotherClientParams();

#endif
    }

#if server
    void OnTriggerExit2D(Collider2D collider2D)
    {
        NetworkVisbility net = GetNetworkVisibility(collider2D);
        if (net != null && connectionToClient != null)
        {
            net.playersObserving.Remove(connectionToClient);
            changedObjects.Add(net);
        }
    }

    private NetworkVisbility GetNetworkVisibility(Collider2D collider2D)
    {
        return collider2D.GetComponent<NetworkVisbility>() ??
               collider2D.gameObject.transform.root.GetComponent<NetworkVisbility>();
    }
#endif
}