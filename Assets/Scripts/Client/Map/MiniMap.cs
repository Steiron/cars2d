﻿#if client
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BonusGame
{
    public string Id;
    public Vector2 Position;
    public Sprite Sprite;

    public BonusGame(GameObject obj)
    {
        Id = obj.GetComponent<NetworkIdentity>().netId.ToString();
        Position = obj.transform.localPosition;
        Sprite = obj.GetComponent<SpriteRenderer>().sprite;
        Debug.Log(Id);
    }
}

public class MiniMap : Singeltons<MiniMap>
{
  /*  public Image MiniMapBackground;
    public Image PlayerImage;
    public Image EatImage;
    public Transform EatParent;
    private Transform player;

    public List<GameObject> AllBonusGameObjects = new List<GameObject>();
    private List<Image> _allEatImages = new List<Image>();

    void Awake()
    {
        //AllBonusGameObjects.Add(new BonusGame(GameObject.FindGameObjectWithTag("Bonus")));
        InvokeRepeating("UpdatePlayerOnMap", 0, 1);
        AllBonusGameObjects = GameObject.FindGameObjectsWithTag("Bonus").ToList();
        /*  GameObject[] t = GameObject.FindGameObjectsWithTag("Bonus");
          foreach (GameObject o in t)
          {
              AllBonusGameObjects.Add(new BonusGame(o));
          }
    }

    void Start()
    {
        SetBonusOnMap();
    }

    private void SetBonusOnMap()
    {
        foreach (var bonusList in AllBonusGameObjects)
        {
            Image bonusImage = Instantiate(EatImage, bonusList.transform.position, Quaternion.identity) as Image;
            if (bonusImage == null) continue;

            bonusImage.name = bonusList.GetComponent<NetworkIdentity>().netId.ToString();
            bonusImage.sprite = bonusList.GetComponent<SpriteRenderer>().sprite;
            bonusImage.transform.SetParent(EatParent);
            bonusImage.transform.localPosition = bonusList.transform.position/6;

            _allEatImages.Add(bonusImage);
        }
    }

    public void SetTarget(Transform entity)
    {
        player = entity;
    }

    public void SetSprite(Sprite newSprite)
    {
        PlayerImage.sprite = newSprite;
    }

    public void SetEatSprite(Sprite newSprite)
    {
        EatImage.sprite = newSprite;
    }

    public void UpdateBonusOnMiniMap(string name)
    {
        int index = name.IndexOf("#", StringComparison.Ordinal)+1;

        Debug.Log(index);
        string _id = name.Substring(index, name.Length - index);

          var t = _allEatImages.Select(image => image).FirstOrDefault( image => image.name.Equals(_id));
          // var t = _allEatImages.Select(image => image).FirstOrDefault(image => AllBonusGameObjects.Find(game => game.Id.Equals(_id)));
          if (t != null)
              t.transform.localPosition = gameObject.transform.position/6;
          Debug.Log(t);
          //SetBonusOnMap();
    }

    private void UpdatePlayerOnMap()
    {
        PlayerImage.transform.localPosition = player.transform.localPosition/6f;
        PlayerImage.transform.localRotation = player.transform.localRotation;
    }

    void Update()
    {
        // UpdatePlayerOnMap();
    }*/
}
#endif