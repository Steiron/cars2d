﻿#if client
using UnityEngine;
using System.Collections;

public class PlayerCamera : Singeltons<PlayerCamera>
{
    private Transform _player;

    public Camera Camera
    {
        get { return GetComponent<Camera>(); }
    }

    void Update()
    {
        CameraZoom();
    }

    void LateUpdate()
    {
        if (_player == null) return;
        var pos = _player.transform.position;
        pos.z = -1;
        Camera.transform.position = pos;

        Camera.orthographicSize = Mathf.Lerp(Camera.orthographicSize, zoom, Time.deltaTime * zoomSpeed);
    }

    public void SetTarget(Transform entity)
    {
        _player = entity;
    }

    
    private float zoom;
    public float zoomSensitivity = 15.0f;
    public float zoomMin = 3.0f;
    public float zoomMax = 6.5f;
    public float zoomSpeed = 5.0f;

    void CameraZoom()
    {
        zoom -= Input.GetAxis("Mouse ScrollWheel")*zoomSensitivity;
        zoom= Mathf.Clamp(zoom, zoomMin, zoomMax);
    }
}
#endif