﻿using UnityEngine;
using UnityEngine.Networking;

#if client
public partial class PlayerStats
{
    public override void OnStartLocalPlayer()
    {
        //MiniMap.Instance.SetSprite(CurrentSprite);
        // Destroy(_colliders);
        Cmd_PlayerActive();

        SetClientUI();

        PlayerUi.SetHealthUI();
        PlayerUi.SetName();
    }
    
    private void SetClientUI()
    {
        Score.Instance.SetScore(Mass);
        // MiniMap.Instance.SetSprite(CurrentSprite);
    }

    
    public void SetAnotherClientParams()
    {
        PlayerUi.SetHealthUI();
        PlayerUi.SetName();
        CurrentSprite = Resources.Load<Sprite>(string.Format("Sprites/Car/{0}/{1}", Type, SpriteName));
    }

}
#endif