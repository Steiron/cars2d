﻿#if client
using UnityEngine;
using UnityEngine.Networking;

public partial class PlayerMovement
{
    public override void OnStartLocalPlayer()
    {
        Instantiate(playerCamera);
        Instantiate(MainPlayerUI);
        Instantiate(Background);
        PlayerCamera.Instance.SetTarget(transform);
       // MiniMap.Instance.SetTarget(transform);
    }

    Quaternion RotationClient()
    {
        Vector3 mousePos = PlayerCamera.Instance.Camera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 dir = mousePos - transform.position;

        float angle = Mathf.Atan2(dir.y, dir.x)*Mathf.Rad2Deg;

        return Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void ChangeCamera()
    {
        PlayerStats.Cmd_SetCameraSize(PlayerCamera.Instance.Camera.orthographicSize);
    }

    [ClientCallback]
    void TransmitRotation()
    {
        if (!isLocalPlayer) return;
        CmdServerUpdateRotation(RotationClient());
    }
}
#endif