﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Networking;

public class Manager : NetworkManager
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _map;
    public GameObject PlayerManager;

    void Start()
    {
        base.connectionConfig.IsAcksLong = true;
        base.connectionConfig.MaxSentMessageQueueSize = 2048;
        base.connectionConfig.PacketSize = 1200;
    }

    public override void OnStartHost()
    {
        ClientScene.RegisterPrefab(PlayerManager);
        ClientScene.RegisterPrefab(_playerPrefab);
    }

    public override void OnStartClient(NetworkClient client)
    {
        ClientScene.RegisterPrefab(_playerPrefab);
        ClientScene.RegisterPrefab(_map);
        ClientScene.RegisterPrefab(PlayerManager);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        if (!clientLoadedScene)
        {
            ClientScene.Ready(conn);
            ClientScene.AddPlayer(0);
        }
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        ClientScene.Ready(conn);

        bool addPlayer = (ClientScene.localPlayers.Count == 0);
        bool foundPlayer = ClientScene.localPlayers.Any(playerController => playerController.gameObject != null);
        if (!foundPlayer)
        {
            addPlayer = true;
        }
        if (addPlayer)
        {
            ClientScene.AddPlayer(0);
        }
    }


#if server
    public override void OnServerSceneChanged(string sceneName)
    {
        if (sceneName != onlineScene) return;

        GameObject networkManager = Instantiate(PlayerManager);
        NetworkServer.Spawn(networkManager);

        GameObject map = Instantiate(_map);
        NetworkServer.Spawn(map);
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        GameObject player =
            Instantiate(_playerPrefab, Map.Instance.RandomMapSpawnPosition, Quaternion.identity) as GameObject;

        if (player == null) return;
        player.GetComponent<PlayerStats>().Type = (CharacterType) Random.Range(1, 3);
        NetworkServer.AddPlayerForConnection(conn, player, 0);

        PlayersManager.Instance.AddPlayer(player, conn.connectionId, "Test", (Colors) Random.Range(1,12));

        NetworkServer.Spawn(player);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        foreach (var p in conn.playerControllers)
        {
            if (p != null && p.gameObject != null)
            {
                PlayersManager.Instance.RemovePlayer(p.gameObject);
            }
        }
        
        base.OnClientDisconnect(conn);
    }
#endif
}