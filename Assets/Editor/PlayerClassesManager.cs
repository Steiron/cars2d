﻿#if UNITY_EDITOR && server
using UnityEditor;

public class PlayerClassesManager
{
    [MenuItem("Assets/Create/Custom/PlayerClasses")]
    public static void CreatePlayer()
    {
        ScriptableObjectUtility.CreateAsset<PlayerClass>();
    }
    
    [MenuItem("Assets/Create/Custom/Bonus")]
    public static void CreateBonus()
    {
        ScriptableObjectUtility.CreateAsset<Bonus>();
    }
}
#endif